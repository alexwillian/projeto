@extends('templates.default')
@section('title')
  Usuários
@endsection

@section('content')

  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-12">
        <table class="table table-bordered table-hover">
          <thead class="thead-light">
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Nome</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>José</td>
            </tr>
            <tr>
              <td>2</td>
              <td>Maria</td>
            </tr>
            <tr>
              <td>3</td>
              <td>Pedro</td>
            </tr>
            <tr>
              <td>4</td>
              <td>João</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="col">
        <p>Hi! This is a list of users.</p>
      </div>
    </div>
  </div>


@endsection
