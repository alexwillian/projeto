@extends('templates.default')
@section('title')
  Lembretes | Nova Entrada
@endsection

@section('content')

  <div class="container">

    <div class="row">
      <div class="col-md-6 col-sm-12">
        <form action="{{ route('lembretes.store') }}" method="post">
          {{ csrf_field() }}

          <div class="form-group">
            <label for="exampleFormControlTextarea1">Novo lembrete</label>
            <textarea class="form-control rounded-0" id="lembrete" name="lembrete" rows="10"></textarea>
          </div>

          <input class="btn btn-primary" type="submit" value="Enviar">
          <input class="btn btn-warning" type="reset" value="Resetar">

        </form>
      </div>
    </div>
    
  </div>


@endsection
