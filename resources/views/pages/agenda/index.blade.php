@extends('templates.default')
@section('title')
  Agenda
@endsection

@section('content')

  <div class="container">
    <div class="row">
      <div class="col-md-10 col-sm-12">
        <table class="table table-bordered table-hover">
          <thead class="thead-light">
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Nome</th>
              <th scope="col">Endereço</th>
              <th scope="col">E-mail</th>
              <th scope="col">Telefone</th>
              <th scope="col">Ações</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($entradas as $value)
              <tr>
                <td>{{ $value->id }}</td>
                <td>{{ $value->nome }}</td>
                <td>{{ $value->endereco }}</td>
                <td>{{ $value->email }}</td>
                <td>{{ $value->telefone }}</td>
                <td>
                  <div class="btn-group" role="group" aria-label="Basic example">
                    {{-- <a href="#" class="btn btn-primary btn-lg btn-sm disabled" role="button" aria-pressed="true">Mostrar</a> --}}
                    <a href="{{ URL::to('agenda/' . $value->id . '/edit') }}" class="btn btn-info btn-lg btn-sm" role="button" aria-pressed="true">Editar</a>
                    {{-- <a href="" class="btn btn-danger btn-lg btn-sm" role="button" aria-pressed="true">Excluir</a> --}}
                    <form class="" action="{{ URL::to('agenda/' . $value->id) }}" method="post">
                        @method('DELETE')
                        @csrf
                        <input class="btn btn-danger btn-sm" type="submit" value="Excluir">
                    </form>

                  </div>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <div class="col-md-2 col-sm-12">
        <a href="{{ URL::to('agenda/create') }}" class="btn btn-success btn-lg btn-sm" role="button" aria-pressed="true" style="margin-top: 2em;">Nova Entrada</a>
        <a href="{{ URL::to('/') }}" class="btn btn-dark btn-lg btn-sm" role="button" aria-pressed="true" style="margin-top: 2em;">Voltar</a>
      </div>
    </div>
  </div>


@endsection
