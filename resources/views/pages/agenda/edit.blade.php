@extends('templates.default')
@section('title')
  Agenda | Editar | {{$entrada->id}}
@endsection

@section('content')

  <div class="container">

    <div class="row">
      <div class="col-md-6 col-sm-12">
        <form action="{{ route('agenda.update', $entrada->id) }}" method="post">
          @method('PUT')
          @csrf

          <div class="form-group">
            <label for="exampleFormControlInput1">Nome</label>
            <input type="text" class="form-control" id="nome" name="nome" value="{{ $entrada->nome }}">
          </div>
          <div class="form-group">
            <label for="exampleFormControlInput2">Endereço</label>
            <input type="text" class="form-control" id="endereco" name="endereco" value="{{ $entrada->endereco }}">
          </div>
          <div class="form-group">
            <label for="exampleFormControlInput3">E-mail</label>
            <input type="email" class="form-control" id="email" name="email" value="{{ $entrada->email }}">
          </div>
          <div class="form-group">
            <label for="exampleFormControlInput4">Telefone</label>
            <input type="string" class="form-control" id="telefone" name="telefone" value="{{ $entrada->telefone }}">
          </div>

          <input class="btn btn-primary" type="submit" value="Submit">
          <input class="btn btn-warning" type="reset" value="Reset">

        </form>

      </div>
    </div>
  </div>


@endsection
