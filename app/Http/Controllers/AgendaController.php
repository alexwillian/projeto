<?php

namespace App\Http\Controllers;

use App\Agenda;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;


class AgendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entradas = Agenda::all();

        return view('pages.agenda.index', compact('entradas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('pages.agenda.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $rules = array(
        'nome'      => 'required|string|max:255',
        'endereco' => 'required|string|max:255',
        'email'      => 'required|string|max:255',
        'telefone'   => 'required|string|max:255',
      );

      $validator = validator::make(Input::all(), $rules);

      if ($validator->fails()) {
        return Redirect::to('/agenda')
        ->withErrors($validator);
        // dd($validator);
      } else {
        // store
        $entrada = new Agenda;
        $entrada->nome        = Input::get('nome');
        $entrada->endereco    = Input::get('endereco');
        $entrada->email       = Input::get('email');
        $entrada->telefone    = Input::get('telefone');
        $entrada->save();

        // redirect
        Session::flash('message', 'Sucesso!');
        return Redirect::to('/agenda');
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Agenda  $agenda
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $entrada = Agenda::find($id);

      return view('pages.agenda.edit')->with('entrada', $entrada);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Agenda  $agenda
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $rules = array(
        'nome'      => 'required|string|max:255',
        'endereco' => 'required|string|max:255',
        'email'      => 'required|string|max:255',
        'telefone'   => 'required|string|max:255',
      );

      $validator = validator::make(Input::all(), $rules);

      if ($validator->fails()) {
        return Redirect::to('/agenda')
        ->withErrors($validator);
        // dd($validator);
      } else {
        // store
        $entrada = Agenda::find($id);
        $entrada->nome        = Input::get('nome');
        $entrada->endereco    = Input::get('endereco');
        $entrada->email       = Input::get('email');
        $entrada->telefone    = Input::get('telefone');
        $entrada->save();

        // redirect
        Session::flash('message', 'Sucesso!');
        return Redirect::to('/agenda');
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Agenda  $agenda
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      // delete
      $del = Agenda::find($id);
      $del->delete();

      // redirect
      Session::flash('message', 'Removido com sucesso!');
      return Redirect::to('/agenda');    }
}
