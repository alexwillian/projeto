<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
                 'name' => 'Julio Kirk',
                 'email' => 'kirk@mail.com',
                 'password' => bcrypt('enterprise'),
             ]);
    }
}
